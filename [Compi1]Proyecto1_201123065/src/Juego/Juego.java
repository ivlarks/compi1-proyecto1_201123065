
package juego;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class Juego extends Canvas implements Runnable{
    
    private static JFrame ventana;
    private final int ANCHO,ALTO;
    private final String nombre;
    
    private static int aps=0;
    private static int fps=0;
    
    private static volatile boolean enFuncionamiento=false;
    private static Thread thread;
    
    
    public Juego(String texto,int ancho,int alto){
        this.ANCHO=ancho*50+10;
        this.ALTO=alto*50+10;
        this.nombre=texto;
        
        setPreferredSize(new Dimension(ANCHO, ALTO));
        
        ventana= new JFrame(nombre);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.setResizable(false);
        ventana.setLayout(new BorderLayout());
        ventana.add(this,BorderLayout.CENTER);
        ventana.pack();
        ventana.setLocationRelativeTo(null);
        ventana.setVisible(true);
        
    }
    private void actualizar(){
        aps++;
    }
    
    private void mostrar(){
        fps++;
    }
    
    public synchronized void iniciar(){
        enFuncionamiento= true;
        thread = new Thread(this,"Graficos");
        thread.start();
    }
    private synchronized void detener(){
        try {
            enFuncionamiento=false;
            thread.join();
        } catch (InterruptedException ex) {
        }
    }
    
    public void run() {
        final int NS_POR_SEG=1000000000;
        final byte APS_OBJETO=60;
        final double NS_POR_ACTUALIZACION=NS_POR_SEG/APS_OBJETO;
        
        long referenciaActualizacion = System.nanoTime();
        long referenciaContador = System.nanoTime();
        double tiempoTranscurrido;
        double delta=0;
        
        while(enFuncionamiento){
            final long inicioBucle = System.nanoTime();
            
            tiempoTranscurrido= inicioBucle-referenciaActualizacion;
            referenciaActualizacion= inicioBucle;
            delta+=tiempoTranscurrido/NS_POR_ACTUALIZACION;
            
            while(delta>=1){
                actualizar();
                delta--;
            }
            mostrar();
            if(System.nanoTime()-referenciaContador>NS_POR_SEG){
                ventana.setTitle(nombre + " || APS:"+aps+"||FPS:"+fps);
                aps=0;
                fps=0;
                referenciaContador=System.nanoTime();
            }
        }
        
    }
}
