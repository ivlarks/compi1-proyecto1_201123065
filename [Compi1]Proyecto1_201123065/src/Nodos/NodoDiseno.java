package Nodos;

public class NodoDiseno {
    String Nombre,imagen,tipo;
    int Puntaje;
    NodoDiseno siguiente, anterior;
    
    public NodoDiseno(String tipo,String nombre,String imagen, int Puntaje){
        this(tipo,nombre,imagen,Puntaje,null,null);
        
    }
    
    public NodoDiseno(String tipo,String nombre,String imagen, int Puntaje,NodoDiseno sig, NodoDiseno ant) {
        this.Nombre=nombre;
        this.imagen=imagen;
        this.tipo=tipo;
        this.Puntaje=Puntaje;
        this.siguiente=sig;
        this.anterior=ant;
    }
    
}
