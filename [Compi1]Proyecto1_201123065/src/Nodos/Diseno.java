/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Nodos;

/**
 *
 * @author marcosmayen
 */
public class Diseno {
    private NodoDiseno inicio,fin,aux;
    public Diseno(){
        inicio=fin=aux=null;
    }
    private Boolean estaVacia(){
        return (inicio==null);
    }
    public void Agregar(String tipo,String nombre,String imagen, int Puntos){
        if(!estaVacia()){
            aux=fin;
            fin= new NodoDiseno(tipo, nombre, imagen, Puntos, aux,null);
            aux.siguiente=fin;
            fin.siguiente=null;
        }else{
            inicio=fin=aux=new NodoDiseno(tipo, nombre, imagen, Puntos);
        }
    }
    
    
    public boolean retExiste(String nombre,String tipo){
        return Existe(nombre,tipo, inicio);
    }
    
    private boolean Existe(String nombre,String tipo,NodoDiseno aux){
        return (aux.Nombre.equals(nombre)&&aux.tipo.equals(tipo))?true:(aux.siguiente==null)?false:Existe(nombre,tipo, aux.siguiente);
    }
    
    public String retornaNodos(){
        String cad = "";
        aux=inicio;
        while(aux!=null){
            cad=cad+"("+aux.Nombre+","+aux.imagen+","+aux.tipo+","+aux.Puntaje+")\n";
            aux=aux.siguiente;
        }
        return cad;
    }
}
