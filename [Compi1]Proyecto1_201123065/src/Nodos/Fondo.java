
package Nodos;

public class Fondo {
    private NodoFondo inicio,fin,aux;
    public Fondo(){
        inicio=fin=aux=null;
    }
    private Boolean estaVacia(){
        return (inicio==null);
    }
    public void Agregar(String nombre, String imagen){
        if(!estaVacia()){
            aux=fin;
            fin= new NodoFondo(nombre, imagen,aux,null);
            aux.siguiente=fin;
            fin.siguiente=null;
        }else{
            inicio=fin=aux=new NodoFondo(nombre, imagen);
        }
    }
    public boolean retExiste(String nombre){
        return Existe(nombre, inicio);
    }
    
    private boolean Existe(String nombre,NodoFondo aux){
        return (aux.nombre.equals(nombre))?true:(aux.siguiente==null)?false:Existe(nombre, aux.siguiente);
    }
    
    public String retornaNodos(){
        String cad = "";
        aux=inicio;
        String auxilio;
        while(aux!=null){
            auxilio="("+aux.nombre+","+aux.imagen+")";
            cad=(cad.equals(""))?auxilio:cad+"\n"+auxilio;
            aux=aux.siguiente;
        }
        return cad;
    }
    
}
