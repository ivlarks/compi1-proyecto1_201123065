
package Nodos;

/**
 *
 * @author marcosmayen
 */
public class NodoEnemigo {
    String nombre,descripcion,imagen;
    int vida,destruir;
    NodoEnemigo anterior,siguiente;
    boolean tipo;
    
    public NodoEnemigo(Boolean tipo,String nombre, int vida,String imagen,int destruir,String descripcion){
        this(tipo, nombre,vida,imagen,destruir,descripcion,null,null);
    }
    
    public NodoEnemigo(boolean tipo,String nombre, int vida,String imagen,int destruir,String descripcion,NodoEnemigo anterior, NodoEnemigo siguiente ) {
        this.tipo=tipo;
        this.nombre=nombre;
        this.vida=vida;
        this.imagen=imagen;
        this.destruir=destruir;
        this.descripcion=descripcion;
        this.anterior=anterior;
        this.siguiente= siguiente;
    }
    
    
}
