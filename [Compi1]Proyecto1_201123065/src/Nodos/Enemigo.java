/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Nodos;

/**
 *
 * @author marcosmayen
 */
public class Enemigo {
    private NodoEnemigo inicio,fin,aux;
    public Enemigo(){
        inicio=fin=aux=null;
    }
    private Boolean estaVacia(){
        return (inicio==null);
    }
    public void Agregar(Boolean tipo,String nombre, int vida,String imagen,int destruir, String descripcion){
        if(!estaVacia()){
            aux=fin;
            fin= new NodoEnemigo(true, nombre, vida, imagen, destruir, descripcion, fin, null);
            aux.siguiente=fin;
            fin.siguiente=null;
        }else{
            inicio=fin=aux=new NodoEnemigo(tipo, nombre, vida, imagen, destruir, descripcion);
        }
    }
    
    
    public boolean retExiste(String nombre){
        return Existe(nombre, inicio);
    }
    
    private boolean Existe(String nombre,NodoEnemigo aux){
        return (aux.nombre.equals(nombre))?true:(aux.siguiente==null)?false:Existe(nombre, aux.siguiente);
    }
    public boolean HeroeVillano(String nombre,boolean tipo){
        return (aux.nombre.equals(nombre)&&aux.tipo==tipo)?true:(aux.siguiente==null)?false:Existe(nombre, aux.siguiente);
    }
    
    public String retornaNodos(){
        String cad = "";
        aux=inicio;
        String tipo;
        while(aux!=null){
            tipo=(aux.tipo)?"heroe":"villano";
            String Scad="("+aux.nombre+","+aux.vida+","+aux.imagen+","+tipo+","+aux.destruir+","+aux.descripcion+")";
            cad=(cad.equals(""))?Scad:cad+"\n"+Scad;
            aux=aux.siguiente;
        }
        return cad;
    }
}
