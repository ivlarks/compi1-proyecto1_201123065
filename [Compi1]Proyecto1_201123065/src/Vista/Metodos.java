/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JOptionPane;

/**
 *
 * @author marcosmayen
 */
public class Metodos {
    
    File f;
    FileReader LeerArchivos;
    public String leer(String nombre){
        
        try{
            f= new File(nombre);
            LeerArchivos= new FileReader(f);
            BufferedReader br = new BufferedReader(LeerArchivos);
            String l = "";
            String aux ="";
            while(true){
                aux=br.readLine();
                if(aux!=null){
                    l=l+aux+"\n";
                }else{
                    break;
                }
            }
            br.close();
            LeerArchivos.close();
            return l;
            
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"algo esta mal con el archivo");
        }
        return null;
    }
    public String leerGrafico(){
        javax.swing.JFileChooser j = new javax.swing.JFileChooser();
        j.showOpenDialog(j);
        String path =j.getSelectedFile().getAbsolutePath();
        try{
            f= new File(path);
            LeerArchivos= new FileReader(f);
            BufferedReader br = new BufferedReader(LeerArchivos);
            String l = "";
            String aux ="";
            while(true){
                aux=br.readLine();
                if(aux!=null){
                    l=l+aux+"\n";
                }else{
                    break;
                }
            }
            br.close();
            LeerArchivos.close();
            return l;
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"algo esta mal con el archivo");
        }
        return null;
            
        
    }
    
}
