
package Vista;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Creditos {
    
    public Creditos() {
        
        JFrame frame = new JFrame("Creditos");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JLabel Nombre = new JLabel("Marcos Daniel");
        JLabel Apellido = new JLabel("Mayen de Leon");
        JLabel id = new JLabel("2011-23065");
        JLabel tipo = new JLabel("[Compi1]");
        JLabel rpg = new JLabel("RPG");
        JPanel panel = new JPanel();
        panel.setBounds(100, 100,100, 100);
        panel.add(tipo);
        panel.add(rpg);
        panel.add(Nombre);
        panel.add(Apellido);
        panel.add(id);
        frame.add(panel);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int alto=dim.width/2-frame.getSize().width/2;
        int ancho=dim.height/2-frame.getSize().height/2;
        frame.setBounds(alto, ancho, 150, 150);
        frame.setVisible(true);
       
    }
}
