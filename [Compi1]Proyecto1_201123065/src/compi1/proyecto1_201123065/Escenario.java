
package compi1.proyecto1_201123065;

import JuegoConfig.ConfigEscenario;
import JuegoConfig.Extras;
import JuegoConfig.Pared;
import Nodos.Diseno;
import Nodos.Enemigo;
import Nodos.Fondo;
import JuegoConfig.Personaje;
import JuegoConfig.Suelo;


public class Escenario {
    private int pos,fila,Token,nTokens;
    private String Error,TokenList,cadena,nombreE,fondoE;
    private int anchoE,altoE;
    private boolean HeroeExiste;
    private ConfigEscenario CONFIG;
    private  Diseno D;
    private  Enemigo E;
    private  Fondo F;
    private  Personaje personaje;
    private  Pared pared;
    private  Suelo suelo;
    private  Extras extra;
    private  String meta;
    private  int mx,my;
    public void inicio(String cadena,Diseno D, Enemigo E, Fondo F){
        CONFIG= new ConfigEscenario();
        HeroeExiste=false;
        personaje= new Personaje();
        pared = new Pared();
        suelo= new Suelo();
        extra= new Extras();
        mx=0;my=0;
        this.D=D;
        this.E=E;
        this.F=F;
        this.cadena=cadena;
        meta="";
        TokenList="";
        nombreE=fondoE="";
        anchoE=altoE=0;
        Error="";
        pos=fila=Token=nTokens=0;
        fila++;
        init();
        
    }
    
    private void init(){
        String subc="";
        String error="";
        if(cadena.length()>pos){
            while(cadena.length()>pos){
                while(cadena.charAt(pos)!='<'&&cadena.length()-1>pos){
                   if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                        if(cadena.charAt(pos)=='\n'){
                            fila++;Token=0;
                        }else{
                            error=error+cadena.charAt(pos);
                        }
                    }
                    pos++;
                }
                if(!error.equals("")){
                    sumaT();
                    setError(error+"no se esperaba fila:"+fila+" columna:"+Token);
                }
                if(cadena.charAt(pos)=='<'){
                    pos++;
                    if(cadena.length()>pos){
                        subc=cierraMayor();
                        if(!subc.equals("")){
                            if(cadena.length()>pos){
                                if(cadena.charAt(pos)=='>'){
                                    EscenarioNombre=EscenarioFondo=EscenarioAncho=EscenarioAlto="";
                                    int sig=0;
                                    if(subc.length()>sig){
                                        String esc="";
                                        while(subc.length()-1>sig&&subc.charAt(sig)!=' '){
                                            esc=esc+subc.charAt(sig);
                                            sig++;
                                        }
                                        if(esc.equals("escenario")){
                                            HeroeExiste=false;
                                            sumaT();
                                            setTokensList("<escenario>", fila, pos);
                                            sig++;
                                            evaluaEscenario(subc,sig);
                                            if(EscenarioNombre.equals("")||EscenarioFondo.equals("")||EscenarioAncho.equals("")||EscenarioAlto.equals("")){
                                                String falta="";
                                                if(EscenarioNombre.equals("")){
                                                    falta=falta+"escenario ";
                                                }
                                                if(EscenarioFondo.equals("")){
                                                    falta=falta+"fondo ";
                                                }
                                                if(EscenarioAncho.equals("")){
                                                    falta=falta+"ancho ";
                                                }
                                                if(EscenarioNombre.equals("")){
                                                    falta=falta+"alto ";
                                                }
                                                setError("falta esta(s) configuraciones: "+falta);
                                            }else{
                                                pos++;
                                                cierreEscenario();
                                               // try{
                                                System.out.println(extra.retornaNodos());
                                                if(personaje.ret()==""||pared.retornaNodos()==""||suelo.retornaNodos()==""||extra.retornaNodos()==""||meta.equals("")){
                                                    setError("falta alguna configuracion, favor verificar");
                                                }else{
                                                    try{
                                                    CONFIG.Agregar(EscenarioNombre, fondoE, anchoE, altoE, 
                                                            personaje, pared, suelo, extra, meta, mx, my);
                                                
                                                    personaje=new Personaje();
                                                    pared=new Pared();
                                                    suelo=new Suelo();
                                                    extra=new Extras();
                                                    meta="";
                                                    mx=0;my=0;
                                                    
                                                    }catch(Exception e){
                                                        setError("algun valor no coincide en el Escenario "+EscenarioNombre);
                                                    }
                                                }
                                            }
                                            
                                            
                                        }else{
                                            sumaT();
                                            setError("se esperaba 'escenario' y sus configuraciones fila:"+fila+" columna:"+Token);
                                        }
                                    }
                                }else{
                                    sumaT();setError("la configuracion no se cerro correctamente fila:"+fila+" columna:"+Token);
                                }
                            }else{sumaT();setError("la configuracion no se cerro correctamente fila:"+fila+" columna:"+Token);}
                        }else{
                            setError("parece que no posee ninguna configuracion");
                        }
                    }else{
                        sumaT();
                        setError("parece que no se cerro la configuracion fila:"+fila+" columna:"+Token);
                    }
                    
                }else{
                    setError("parece que no se abrió ninguna configuracion");
                }
                pos++;
            }
            
        }else{
            setError("parece que el archivo no posee ninguna configuracion");
        }
    }
    private void cierreEscenario(){
        String error="";
        String subc="";
        if(cadena.length()>pos){
            while(cadena.charAt(pos)!='<'&&cadena.length()-1>pos){
                if(cadena.charAt(pos)!='\t'&&cadena.charAt(pos)!=' '){
                    if(cadena.charAt(pos)=='\n'){
                        fila++;Token=0;
                    }else{
                        error=error+cadena.charAt(pos);
                    }
                }
                pos++;
            }
            if(!error.equals("")&&!error.equals(" ")){
                sumaT();
                setError(error+" no se esperaba fila:"+fila+" columna:"+Token);
            }
            if(cadena.length()>pos){
                subc="";
                if(cadena.charAt(pos)=='<'){
                    pos++;
                    subc=cierraMayor();
                    if(cadena.length()>pos){
                        if(cadena.charAt(pos)=='>'){
                            pos++;
                            sumaT();
                            switch(subc){
                                case "/escenario":
                                    setTokensList("</escenario>", fila, pos);
                                    break;
                                case "personajes":
                                    setTokensList("<personajes>", fila, pos);
                                    pos++;
                                    if(cadena.length()>pos){
                                        configPersonajes();
                                    }else{
                                        sumaT();
                                        setError("se esperaba </personajes></escenario> fila:"+fila+" columna:"+Token);
                                    }
                                    break;
                                    
                                    
                                case "paredes":
                                    setTokensList("<paredes>", fila, pos);
                                    if(cadena.length()>pos){
                                        String conf="";//configuracion dentro de las llaves
                                        
                                        while(cadena.charAt(pos)!='<'&&cadena.length()-1>pos){
                                            conf=conf+cadena.charAt(pos);
                                            pos++;
                                        }
                                        if(cadena.charAt(pos)=='<'){
                                            pos++;
                                            String config="";
                                            if(cadena.length()>pos){
                                                while(cadena.length()-1>pos&&cadena.charAt(pos)!='>'){
                                                    config=config+cadena.charAt(pos);
                                                    pos++;
                                                }
                                                
                                                if(cadena.charAt(pos)=='>'){
                                                    pos++;
                                                    if(config.equals("/paredes")){
                                                        ConfigParedes(conf,0);
                                                    }else{
                                                        sumaT();
                                                        setError(" se esperaba </paredes> no "+config+" fila:"+fila+" columna:"+Token);
                                                    }
                                                    
                                                }else{
                                                    sumaT();
                                                    setError("falta la llave '>' fila:"+fila+" columna:"+Token);
                                                }
                                            }else{
                                                sumaT();
                                                setError("se esperaba</suelo></escenario> fila:"+fila+" columna:"+Token);
                                                
                                            }
                                        }else {
                                            sumaT();
                                            setError("se esperaba</suelo></escenario> fila:"+fila+" columna:"+Token);
                                        }
                                    }else{
                                        sumaT();
                                        setError("se esperaba </paredes></escenario> fila:"+fila+" columna:"+Token);
                                    }
                                    
                                    
                                    
                                    break;
                                case "suelo":
                                    setTokensList("<suelo>", fila, pos);
                                    if(cadena.length()>pos){
                                        String conf="";//configuracion dentro de las llaves
                                        
                                        while(cadena.charAt(pos)!='<'&&cadena.length()-1>pos){
                                            conf=conf+cadena.charAt(pos);
                                            pos++;
                                        }
                                        if(cadena.charAt(pos)=='<'){
                                            pos++;
                                            String config="";
                                            if(cadena.length()>pos){
                                                while(cadena.length()-1>pos&&cadena.charAt(pos)!='>'){
                                                    config=config+cadena.charAt(pos);
                                                    pos++;
                                                }
                                                
                                                if(cadena.charAt(pos)=='>'){
                                                    pos++;
                                                    if(config.equals("/suelo")){
                                                        ConfigSuelo(conf,0);
                                                        sumaT();
                                                        setTokensList("</suelo>", fila, pos);
                                                    }else{
                                                        sumaT();
                                                        setError(" se esperaba </suelo> no "+config+" fila:"+fila+" columna:"+Token);
                                                    }
                                                    
                                                }else{
                                                    sumaT();
                                                    setError("falta la llave '>' fila:"+fila+" columna:"+Token);
                                                }
                                            }else{
                                                sumaT();
                                                setError("se esperaba</suelo></escenario> fila:"+fila+" columna:"+Token);
                                                
                                            }
                                        }else {
                                            sumaT();
                                            setError("se esperaba</suelo></escenario> fila:"+fila+" columna:"+Token);
                                        }
                                    }else{
                                        sumaT();
                                        setError("se esperaba </suelo></escenario> fila:"+fila+" columna:"+Token);
                                    }
                                    break;
                                case "extras":
                                    setTokensList("<extras>", fila, pos);
                                    if(cadena.length()>pos){
                                        configuraExtras();
                                    }else{
                                        sumaT();
                                        setError("se esperaba </extras></escenario> fila:"+fila+" columna:"+Token);
                                    }
                                    break;
                                case "meta":
                                    pos++;
                                    sumaT();
                                    String CadenaMeta="";
                                    setTokensList("<meta>", fila, pos);
                                    if(cadena.length()>pos){
                                        while(cadena.charAt(pos)!='<'&&cadena.length()-1>pos){
                                            if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                                                if(cadena.charAt(pos)=='\n'){
                                                    fila++;Token=0;
                                                }else{
                                                    CadenaMeta=CadenaMeta+cadena.charAt(pos);
                                                } 
                                            }
                                            pos++;
                                        }
                                        if(cadena.charAt(pos)=='<'){
                                            pos++;
                                            String subm="";
                                            if(cadena.length()>pos){
                                                subm=cierraMayor();
                                                if(cadena.charAt(pos)=='>'){
                                                    pos++;
                                                    if(subm.equals("/meta")){
                                                        int val=0;
                                                        if(CadenaMeta.length()>val){
                                                            String nom="";
                                                            while(CadenaMeta.length()-1>val && CadenaMeta.charAt(val)!='('){
                                                                nom=nom+CadenaMeta.charAt(val);
                                                                val++;
                                                            }
                                                            if(CadenaMeta.charAt(val)=='('){
                                                                val++;
                                                                if(CadenaMeta.length()>val){
                                                                    String param="";
                                                                    while(CadenaMeta.charAt(val)!=')'&&CadenaMeta.length()-1>val){
                                                                        param=param+CadenaMeta.charAt(val);
                                                                        val++;
                                                                    }
                                                                    if(CadenaMeta.charAt(val)==')'){
                                                                        val++;

                                                                        String arreglo[]=param.split(",");
                                                                        try{
                                                                            if(arreglo.length>=2){
                                                                                mx=Integer.parseInt(arreglo[0]);
                                                                                my=Integer.parseInt(arreglo[1]);
                                                                                meta=nom;
                                                                            }else{
                                                                                setError("el tamaño del arreglo en la meta no esta bien");
                                                                            }
                                                                        }
                                                                        catch(Exception e){
                                                                            setError("la configracion de la meta esta mal, favor revisar");
                                                                        }
                                                                    }else{
                                                                        sumaT();
                                                                        setError("falta simbolo ')' fila:"+fila+" columna:"+Token);
                                                                    }

                                                                }else{
                                                                    sumaT();
                                                                    setError("falta simbolo ')' fila: "+fila+" columna:"+Token);
                                                                }
                                                            }else{
                                                                sumaT();
                                                                setError("falta caracter '(' fila:"+fila+" columna:"+Token);
                                                            }
                                                        }else{
                                                            setError("parece que <meta></meta> no posee configuracion");
                                                        }
                                                        sumaT();
                                                        setTokensList("</meta>", fila, val);
                                                    }else{
                                                        sumaT();
                                                        setError("se esperaba </meta> fila:"+fila+" columna:"+Token);
                                                    }
                                                }else{
                                                    sumaT();
                                                    setError("se esperaba el caracter '>' fila:"+fila+" columna:"+Token);
                                                }
                                            }
                                        }
                                    }else{
                                        sumaT();
                                        setError("se esperaba </meta></escenario> fila:"+fila+" columna:"+Token);
                                    }
                                    break;
                                default:
                                    sumaT();
                                    setError("'"+subc+"'no se esperaba sino </escenario> fila:"+fila+" columna:"+Token);
                                    break;
                            }
                            if(cadena.length()>pos){
                                if(!subc.equals("/escenario")){
                                    cierreEscenario();
                                }
                            }
                        }else{
                            sumaT();
                            setError(" se esperaba el simbolo '>' fila:"+fila+" columna: "+Token);
                        }
                    }else{
                        sumaT();
                        setError(" se esperaba el simbolo '>' fila:"+fila+" columna: "+Token);
                    }
                }else{
                    setError("se esperaba el signo '<' fila:"+fila+" columna"+Token);
                }
            }else{
                sumaT();
                setError("no se cerro la configuracion del escenario fila:"+fila+" columna:"+Token);
            }
        }else{
            sumaT();
            setError("no se cerro la configuracion del escenario fila:"+fila+" columna:"+Token);
        }
    }
    
    private void configuraExtras(){
        String titulo="",error="";
        if(cadena.length()>pos){
            while(cadena.length()-1>pos&&cadena.charAt(pos)!='<'){
                if(cadena.charAt(pos)!='\t'&&cadena.charAt(pos)!=' '){
                    if(cadena.charAt(pos)=='\n'){
                        fila++;Token=0;
                    }else{
                        error=error+cadena.charAt(pos);
                    }
                }
                pos++;
            }
            if(cadena.charAt(pos)=='<'){
                pos++;
                if(cadena.length()>pos){
                    while(cadena.length()-1>pos&&cadena.charAt(pos)!='>'){
                        if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                            if(cadena.charAt(pos)=='\n'){
                                fila++;Token=0;
                            }else{
                                titulo=titulo+cadena.charAt(pos);
                            }
                        }
                        pos++;
                    }
                    if(cadena.charAt(pos)=='>'){
                        String evalua ="";
                        switch(titulo){
                            case "/extras":
                                sumaT();
                                setTokensList("</extras>", fila, pos);
                                break;
                            case "armas":
                                sumaT();
                                setTokensList("<armas>", fila, pos);
                                
                                pos++;
                                if(cadena.length()>pos){
                                    while(cadena.length()-1>pos&&cadena.charAt(pos)!='<'){
                                        if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                                            if(cadena.charAt(pos)=='\n'){
                                                fila++;Token=0;
                                            }else{
                                                evalua=evalua+cadena.charAt(pos);
                                            }
                                        }
                                        pos++;
                                    }
                                    if(cadena.charAt(pos)=='<'){
                                        String valor="";
                                        pos++;
                                        if(cadena.length()>pos){
                                            valor= cierraMayor();
                                            if(cadena.charAt(pos)=='>'){
                                                if(valor.equals("/armas")){
                                                    ConfiArmas(evalua, 0);
                                                    sumaT();
                                                    setTokensList("</armas>", fila, pos);
                                                    
                                                }else{
                                                    sumaT();
                                                    setError(" se esperaba </armas> no "+valor+" fila:"+fila+" columna:"+Token);
                                                }
                                            }else{
                                                sumaT();
                                                setError("falta el simbolo '>' fila:"+fila+" columna:"+Token);
                                            }
                                        }
                                    }else{
                                        sumaT();
                                        setError("se esperaba </armas> fila:"+fila+" columna:"+Token);
                                        
                                    }
                                }else{
                                    sumaT();
                                    setError("se esperaba </armas> fila:"+fila+" columna:"+Token);
                                }
                                
                                break;
                            case "bonus":sumaT();
                                setTokensList("<bonus>", fila, pos);
                                evalua ="";
                                pos++;
                                if(cadena.length()>pos){
                                    while(cadena.length()-1>pos&&cadena.charAt(pos)!='<'){
                                        if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                                            if(cadena.charAt(pos)=='\n'){
                                                fila++;Token=0;
                                            }else{
                                                evalua=evalua+cadena.charAt(pos);
                                            }
                                        }
                                        pos++;
                                    }
                                    if(cadena.charAt(pos)=='<'){
                                        String valor="";
                                        pos++;
                                        if(cadena.length()>pos){
                                            valor= cierraMayor();
                                            if(cadena.charAt(pos)=='>'){
                                                if(valor.equals("/bonus")){
                                                    ConfiBonus(evalua, 0);
                                                    sumaT();
                                                    setTokensList("</bonus>", fila, pos);
                                                    
                                                }else{
                                                    sumaT();
                                                    setError(" se esperaba </bonus> no "+valor+" fila:"+fila+" columna:"+Token);
                                                }
                                            }else{
                                                sumaT();
                                                setError("falta el simbolo '>' fila:"+fila+" columna:"+Token);
                                            }
                                        }
                                    }else{
                                        sumaT();
                                        setError("se esperaba </bonus> fila:"+fila+" columna:"+Token);
                                        
                                    }
                                }else{
                                    sumaT();
                                    setError("se esperaba </bonus> fila:"+fila+" columna:"+Token);
                                }
                                
                                break;
                            default:
                                sumaT();
                                setError("se esperaba </extras> no '<"+titulo+" >'fila:"+fila+" columna:"+Token);
                                break;
                        }
                        pos++;
                        if(cadena.length()>pos){
                            if(!titulo.equals("/extras")){
                                configuraExtras();
                            }
                        }
                    }
                    
                }else{
                    sumaT();
                    setError(" falta '>' fila"+fila+" columna:"+Token);
                    
                }
            }else{
                sumaT();
                setError(" falta '<' fila"+fila+" columna:"+Token);
            }
        }
    }
    private void ConfiBonus(String conf, int val){
        if(conf.length()>val){
            String nombre="";
            String parametros="";
            while(conf.charAt(val)!='('&&conf.length()-1>val){
                nombre=nombre+conf.charAt(val);
                val++;
            }
            if(conf.charAt(val)=='('){
                val++;
                if(conf.length()>val){
                    while(conf.length()>val&&conf.charAt(val)!=')'){
                        parametros=parametros+conf.charAt(val);
                        val++;
                    }
                    if(conf.charAt(val)==')'){
                        val++;
                        if(D.retExiste(nombre, "bonus")){
                            String []arr= parametros.split(",");
                            if(arr.length>=2){
                                String ix="",iy="";
                                try{
                                    int tam;
                                    ix=arr[0];
                                    iy=arr[1];
                                    extra.Agregar(nombre, 
                                            "bonus", 
                                            Integer.parseInt(ix), 
                                            Integer.parseInt(iy));
                                    sumaT();
                                    setTokensList(nombre, fila, val);
                                    sumaT();
                                    setTokensList("(", fila, val);
                                    sumaT();
                                    setTokensList(parametros, fila, val);
                                    sumaT();
                                    setTokensList(")", fila, val);
                                }catch(Exception e){
                                    sumaT();
                                    setError(" parece que la configuracion del tamaño esta mal fila:"+fila+" columna"+Token);
                                    
                                }
                            }else{
                                sumaT();
                                setError("favor revisar la config de bonus fila:"+fila+"columna:"+Token);
                            }
                        }
                        else{
                            setError(nombre+" no existe como bonus fila:"+fila+" columna:"+Token);
                        }
                    }else{
                        setError("se esperaba ')' fila:"+fila+" columna:"+Token);
                    }
                    if(conf.length()>val){
                        if(conf.charAt(val)==';'){
                            val++;
                            if(conf.length()>val){
                                ConfiBonus(conf,val);
                            }
                        }
                        else{
                            sumaT();
                            setError("falta ';' fila:"+fila+" columna:"+Token);
                        }
                    }
                    else{
                        sumaT();
                        setError("falta ';' fila:"+fila+" columna:"+Token);
                    }
                }else{
                    sumaT();
                    setError("falta ')' fila:"+fila+"columna"+Token);
                }
            }else{
                sumaT();
                setError("falta el signo '(' fila:"+fila+" columna:"+Token);
            }
        }else{
            setError(" parece que <bonus></bonus> no tiene ninguna configuracion");
        }
    }
    
    private void ConfiArmas(String conf,int val){
        if(conf.length()>val){
            String nombre="";
            String parametros="";
            while(conf.charAt(val)!='('&&conf.length()-1>val){
                nombre=nombre+conf.charAt(val);
                val++;
            }
            if(conf.charAt(val)=='('){
                val++;
                if(conf.length()>val){
                    while(conf.length()>val&&conf.charAt(val)!=')'){
                        parametros=parametros+conf.charAt(val);
                        val++;
                    }
                    if(conf.charAt(val)==')'){
                        val++;
                        if(D.retExiste(nombre, "arma")){
                            String []arr= parametros.split(",");
                            if(arr.length>=2){
                                String ix="",iy="";
                                try{
                                    int tam;
                                    ix=arr[0];
                                    iy=arr[1];
                                    extra.Agregar(nombre, 
                                            "arma", 
                                            Integer.parseInt(ix), 
                                            Integer.parseInt(iy));
                                    sumaT();
                                    setTokensList(nombre, fila, val);
                                    sumaT();
                                    setTokensList("(", fila, val);
                                    sumaT();
                                    setTokensList(parametros, fila, val);
                                    sumaT();
                                    setTokensList(")", fila, val);
                                }catch(Exception e){
                                    sumaT();
                                    setError(" parece que la configuracion del tamaño esta mal fila:"+fila+" columna"+Token);
                                    
                                }
                            }else{
                                sumaT();
                                setError("favor revisar la config de arma fila:"+fila+"columna:"+Token);
                            }
                        }
                        else{
                            setError(nombre+" no existe como arma fila:"+fila+" columna:"+Token);
                        }
                    }else{
                        setError("se esperaba ')' fila:"+fila+" columna:"+Token);
                    }
                    if(conf.length()>val){
                        if(conf.charAt(val)==';'){
                            val++;
                            if(conf.length()>val){
                                ConfiArmas(conf,val);
                            }
                        }
                        else{
                            sumaT();
                            setError("falta ';' fila:"+fila+" columna:"+Token);
                        }
                    }
                    else{
                        sumaT();
                        setError("falta ';' fila:"+fila+" columna:"+Token);
                    }
                }else{
                    sumaT();
                    setError("falta ')' fila:"+fila+"columna"+Token);
                }
            }else{
                sumaT();
                setError("falta el signo '(' fila:"+fila+" columna:"+Token);
            }
        }else{
            setError(" parece que <arma></arma> no tiene ninguna configuracion");
        }
    }
    
    
    
    private void ConfigSuelo(String conf,int val){
        if(conf.length()>val){
            String nombre="";
            String parametros="";
            while(conf.charAt(val)!='('&&conf.length()-1>val){
                nombre=nombre+conf.charAt(val);
                val++;
            }
            if(conf.charAt(val)=='('){
                val++;
                if(conf.length()>val){
                    while(conf.length()>val&&conf.charAt(val)!=')'){
                        parametros=parametros+conf.charAt(val);
                        val++;
                    }
                    if(conf.charAt(val)==')'){
                        val++;
                        if(D.retExiste(nombre, "suelo")){
                            String []arr= parametros.split(",");
                            if(arr.length>=2){
                                String ix="",iy="",fx="",fy="";
                                try{
                                    int tam;
                                    tam=0;
                                    if(arr[0].contains("..")){
                                        while(arr[0].length()>tam&&arr[0].charAt(tam)!='.'){
                                            ix=ix+arr[0].charAt(tam);
                                            tam++;
                                        }
                                        if(arr[0].charAt(tam)=='.'){
                                            tam++;
                                            if(arr[0].length()>tam){
                                                if(arr[0].charAt(tam)=='.'){
                                                    tam++;
                                                    while(arr[0].length()>tam){
                                                        fx=fx+arr[0].charAt(tam);
                                                        tam++;
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        ix=fx=arr[0];
                                    }
                                    tam=0;
                                    if(arr[1].contains("..")){
                                        while(arr[1].length()>tam&&arr[1].charAt(tam)!='.'){
                                            iy=iy+arr[1].charAt(tam);
                                            tam++;
                                        }
                                        if(arr[1].charAt(tam)=='.'){
                                            tam++;
                                            if(arr[1].length()>tam){
                                                if(arr[1].charAt(tam)=='.'){
                                                    tam++;
                                                    while(arr[1].length()>tam){
                                                        fy=fy+arr[1].charAt(tam);
                                                        tam++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        iy=fy=arr[1];
                                    }
                                    
                                    suelo.Agregar(nombre, 
                                            Integer.parseInt(ix), 
                                            Integer.parseInt(fx), 
                                            Integer.parseInt(iy), 
                                            Integer.parseInt(fy));
                                    sumaT();
                                    setTokensList(nombre, fila, val);
                                    sumaT();
                                    setTokensList("(", fila, val);
                                    sumaT();
                                    setTokensList(parametros, fila, val);
                                    sumaT();
                                    setTokensList(")", fila, val);
                                }catch(Exception e){
                                    sumaT();
                                    setError(" parece que la configuracion del tamaño esta mal fila:"+fila+" columna"+Token);
                                    
                                }
                            }else{
                                sumaT();
                                setError("favor revisar la config de suelo fila:"+fila+"columna:"+Token);
                            }
                        }
                        else{
                            setError(nombre+" no existe como suelo fila:"+fila+" columna:"+Token);
                        }
                    }else{
                        setError("se esperaba ')' fila:"+fila+" columna:"+Token);
                    }
                    if(conf.length()>val){
                        if(conf.charAt(val)==';'){
                            val++;
                            if(conf.length()>val){
                                ConfigSuelo(conf,val);
                            }
                        }
                        else{
                            sumaT();
                            setError("falta ';' fila:"+fila+" columna:"+Token);
                        }
                    }
                    else{
                        sumaT();
                        setError("falta ';' fila:"+fila+" columna:"+Token);
                    }
                }else{
                    sumaT();
                    setError("falta ')' fila:"+fila+"columna"+Token);
                }
            }else{
                sumaT();
                setError("falta el signo '(' fila:"+fila+" columna:"+Token);
            }
        }else{
            setError(" parece que <suelo></suelo> no tiene ninguna configuracion");
        }
    }
    private void ConfigParedes(String conf,int val){
        if(conf.length()>val){
            String nombre="";
            String parametros="";
            while(conf.charAt(val)!='('&&conf.length()-1>val){
                nombre=nombre+conf.charAt(val);
                val++;
            }
            if(conf.charAt(val)=='('){
                val++;
                if(conf.length()>val){
                    while(conf.length()>val&&conf.charAt(val)!=')'){
                        parametros=parametros+conf.charAt(val);
                        val++;
                    }
                    if(conf.charAt(val)==')'){
                        val++;
                        if(D.retExiste(nombre, "pared")){
                            String []arr= parametros.split(",");
                            if(arr.length>=2){
                                String ix="",iy="",fx="",fy="";
                                try{
                                    int tam;
                                    tam=0;
                                    if(arr[0].contains("..")){
                                        while(arr[0].length()>tam&&arr[0].charAt(tam)!='.'){
                                            ix=ix+arr[0].charAt(tam);
                                            tam++;
                                        }
                                        if(arr[0].charAt(tam)=='.'){
                                            tam++;
                                            if(arr[0].length()>tam){
                                                if(arr[0].charAt(tam)=='.'){
                                                    tam++;
                                                    while(arr[0].length()>tam){
                                                        fx=fx+arr[0].charAt(tam);
                                                        tam++;
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        ix=fx=arr[0];
                                    }
                                    tam=0;
                                    if(arr[1].contains("..")){
                                        while(arr[1].length()>tam&&arr[1].charAt(tam)!='.'){
                                            iy=iy+arr[1].charAt(tam);
                                            tam++;
                                        }
                                        if(arr[1].charAt(tam)=='.'){
                                            tam++;
                                            if(arr[1].length()>tam){
                                                if(arr[1].charAt(tam)=='.'){
                                                    tam++;
                                                    while(arr[1].length()>tam){
                                                        fy=fy+arr[1].charAt(tam);
                                                        tam++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        iy=fy=arr[1];
                                    }
                                    
                                    pared.Agregar(nombre, 
                                            Integer.parseInt(ix), 
                                            Integer.parseInt(fx), 
                                            Integer.parseInt(iy), 
                                            Integer.parseInt(fy));
                                    sumaT();
                                    setTokensList(nombre, fila, val);
                                    sumaT();
                                    setTokensList("(", fila, val);
                                    sumaT();
                                    setTokensList(parametros, fila, val);
                                    sumaT();
                                    setTokensList(")", fila, val);
                                }catch(Exception e){
                                    sumaT();
                                    setError(" parece que la configuracion del tamño esta mal fila:"+fila+" columna"+Token);
                                    
                                }
                            }else{
                                sumaT();
                                setError("favor revisar la config de pared fila:"+fila+"columna:"+Token);
                            }
                        }
                        else{
                            setError(nombre+" no existe como pared fila:"+fila+" columna:"+Token);
                        }
                    }else{
                        setError("se esperaba ')' fila:"+fila+" columna:"+Token);
                    }
                    if(conf.length()>val){
                        if(conf.charAt(val)==';'){
                            val++;
                            if(conf.length()>val){
                                ConfigParedes(conf,val);
                            }
                        }
                        else{
                            sumaT();
                            setError("falta ';' fila:"+fila+" columna:"+Token);
                        }
                    }
                    else{
                        sumaT();
                        setError("falta ';' fila:"+fila+" columna:"+Token);
                    }
                }else{
                    sumaT();
                    setError("falta ')' fila:"+fila+"columna"+Token);
                }
            }else{
                sumaT();
                setError("falta el signo '(' fila:"+fila+" columna:"+Token);
            }
        }else{
            setError(" parece que <paredes></paredes> no tiene ninguna configuracion");
        }
        
    }
    
    private void configPersonajes(){
        String sub="",error="";
        String evalua ="";
        if(cadena.length()>pos){
            while(cadena.length()>pos && cadena.charAt(pos)!='<'){
                if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                    if(cadena.charAt(pos)=='\n'){
                        fila++;Token=0;
                    }else{
                        error=error+cadena.charAt(pos);
                    }
                }
                pos++;
            }
            if(cadena.charAt(pos)=='<'){
                pos++;
                if(cadena.length()>pos){
                    sub=cierraMayor();
                    if(cadena.charAt(pos)=='>'){
                        pos++;
                        sumaT();
                        switch(sub){
                                case "/personajes":
                                    setTokensList("</personajes>", fila, pos);
                                    break;
                                case "heroes":
                                    setTokensList("<heroes>", fila, pos);
                                    if(cadena.length()>pos){
                                        while(cadena.length()>pos && cadena.charAt(pos)!='<'){
                                            if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                                                if(cadena.charAt(pos)=='\n'){
                                                    fila++;Token=0;
                                                }else{
                                                    evalua=evalua+cadena.charAt(pos);
                                                }
                                            }
                                            pos++;
                                        }
                                        if(cadena.charAt(pos)=='<'){
                                            pos++;
                                            if(cadena.length()>pos){
                                                sub=cierraMayor();
                                                if(sub.equals("/heroes")){
                                                    int valor=0;
                                                    String nombreP="";
                                                    if(evalua.length()>valor){
                                                        while(evalua.charAt(valor)!='('&&evalua.length()-1>valor){
                                                            nombreP=nombreP+evalua.charAt(valor);
                                                            valor++;
                                                        }
                                                        
                                                        if(evalua.charAt(valor)=='('){
                                                            sumaT();
                                                            valor++;
                                                            String conf="";
                                                            if(evalua.length()>valor){
                                                                if(E.retExiste(nombreP)){
                                                                    String coordenadas="";
                                                                    while(evalua.length()-1>valor&&evalua.charAt(valor)!=')'){
                                                                        coordenadas=coordenadas+evalua.charAt(valor);
                                                                        valor++;
                                                                    }
                                                                    sumaT();
                                                                    if(evalua.charAt(valor)==')'){
                                                                        sumaT();
                                                                        valor++;  
                                                                        setTokensList(nombreP, fila, pos);                                   
                                                                        sumaT();
                                                                        setTokensList("(", fila, pos);                                  
                                                                        sumaT();
                                                                        setTokensList(coordenadas, fila, pos);                                  
                                                                        sumaT();
                                                                        setTokensList(")", fila, pos);
                                                                        
                                                                        if(evalua.length()>valor){
                                                                            if(evalua.charAt(valor)==';'){
                                                                                String casilla[]=coordenadas.split(",");
                                                                                if(casilla.length>=2){
                                                                                    try{
                                                                                        if(HeroeExiste==false){
                                                                                            if(E.HeroeVillano(nombreP, true)){
                                                                                                int x= Integer.parseInt(casilla[0]);
                                                                                                int y = Integer.parseInt(casilla[1]);
                                                                                                personaje.ingresa(true, nombreP, x, y);

                                                                                            }else{
                                                                                                setError("revise si su heroe ya existe");
                                                                                            }
                                                                                            HeroeExiste=true;
                                                                                        }
                                                                                    }catch(Exception e){
                                                                                        setError("los valores de "+nombreP+" (heroe) parecen no ser enteros, favor revisar");
                                                                                    }
                                                                                }
                                                                            }else{
                                                                                sumaT();
                                                                                setError("falta ';' fila:"+fila+" columna:"+Token);
                                                                                
                                                                            }
                                                                        }else{
                                                                            sumaT();
                                                                            setError("falta ';' fila:"+fila+" columna:"+Token);
                                                                        }
                                                                    }
                                                                }else{
                                                                    sumaT();
                                                                    setError(nombreP+" parece que no existe fila:"+fila+" columna"+Token);
                                                                }
                                                            }
                                                        }else{
                                                            sumaT();
                                                            setError(" no se encontro configuracion de heroe fila:"+fila+" columna:"+Token);
                                                        }
                                                    }sumaT();
                                                    setTokensList("</heroes>", fila, pos);
                                                    
                                                }else{
                                                    sumaT();
                                                    setError("se esperaba </heroes> no '<"+sub+">' fila:"+fila+" columna:"+Token);
                                                }
                                                
                                            }else{
                                                sumaT();
                                                setError("se esperaba el simbolo '<' fila:"+fila+" columna:"+Token);
                                                
                                            }
                                        }else{
                                            sumaT();
                                            setError("se esperaba el simbolo '<' fila:"+fila+" columna:"+Token);
                                        }
                                    }
                                    break;
                                case "villanos":
                                    setTokensList("<villanos>", fila, pos);if(cadena.length()>pos){
                                        while(cadena.length()>pos && cadena.charAt(pos)!='<'){
                                            if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                                                if(cadena.charAt(pos)=='\n'){
                                                    fila++;Token=0;
                                                }else{
                                                    evalua=evalua+cadena.charAt(pos);
                                                }
                                            }
                                            pos++;
                                        }
                                        if(cadena.charAt(pos)=='<'){
                                            pos++;
                                            if(cadena.length()>pos){
                                                sub=cierraMayor();
                                                if(sub.equals("/villanos")){
                                                    int valor=0;
                                                    String nombreP="";
                                                    if(valor<evalua.length()){
                                                        while(evalua.charAt(valor)!='('&&evalua.length()-1>valor){
                                                            nombreP=nombreP+evalua.charAt(valor);
                                                            valor++;
                                                        }
                                                        if(evalua.charAt(valor)=='('){
                                                            valor++;
                                                            String conf="";
                                                            if(evalua.length()>valor){
                                                                if(E.retExiste(nombreP)){
                                                                    String coordenadas="";
                                                                    while(evalua.length()-1>valor&&evalua.charAt(valor)!=')'){
                                                                        coordenadas=coordenadas+evalua.charAt(valor);
                                                                        valor++;
                                                                    }
                                                                    if(evalua.charAt(valor)==')'){
                                                                        sumaT();
                                                                        valor++;  
                                                                        setTokensList(nombreP, fila, pos);                                   
                                                                        sumaT();
                                                                        setTokensList("(", fila, pos);                                  
                                                                        sumaT();
                                                                        setTokensList(coordenadas, fila, pos);                                  
                                                                        sumaT();
                                                                        setTokensList(")", fila, pos);
                                                                        if(evalua.length()>valor){
                                                                            if(evalua.charAt(valor)==';'){
                                                                                String casilla[]=coordenadas.split(",");
                                                                                sumaT();
                                                                                if(casilla.length>=2){
                                                                                    try{
                                                                                        if(E.HeroeVillano(nombreP, false)){
                                                                                            int x= Integer.parseInt(casilla[0]);
                                                                                            int y = Integer.parseInt(casilla[1]);
                                                                                            personaje.ingresa(false, nombreP, x, y);
                                                                                        }else{
                                                                                            setError("revise si su villano ya existe");
                                                                                        }
                                                                                    }catch(Exception e){
                                                                                        setError("los valores de"+nombreP+" (villano) parece no ser enteros, favor revisar");
                                                                                    }
                                                                                }
                                                                            }else{
                                                                                sumaT();
                                                                                setError("falta ';' fila:"+fila+" columna:"+Token);
                                                                                
                                                                            }
                                                                        }else{
                                                                            sumaT();
                                                                            setError("falta ';' fila:"+fila+" columna:"+Token);
                                                                        }
                                                                    }
                                                                }else{
                                                                    sumaT();
                                                                    setError(nombreP+" parece que no existe fila:"+fila+" columna"+Token);
                                                                }
                                                            }
                                                        }else{
                                                            sumaT();
                                                            setError(" no se encontro configuracion de villano fila:"+fila+" columna:"+Token);
                                                        }
                                                    }
                                                    sumaT();
                                                    setTokensList("</villanos>", fila, pos);
                                                }else{
                                                    sumaT();
                                                    setError("se esperaba </villanos> no '<"+sub+">' fila:"+fila+" columna:"+Token);
                                                }
                                                
                                            }else{
                                                sumaT();
                                                setError("se esperaba el simbolo '<' fila:"+fila+" columna:"+Token);
                                                
                                            }
                                        }else{
                                            sumaT();
                                            setError("se esperaba el simbolo '<' fila:"+fila+" columna:"+Token);
                                        }
                                    }
                                    break;
                                default:
                                    setError("<"+sub+"> no se esperaba, sino </personajes></escenario> fila:"+fila+" columna:"+Token);
                                    break;
                            }
                        if(cadena.length()>pos){
                            if(!sub.equals("/personajes")){
                                configPersonajes();   
                            }
                        }
                    }else{
                        sumaT();
                        setError("falta el signo cierre fila:"+fila+" columna:"+Token);
                    }
                }else{
                    sumaT();
                    setError("se esperaba el simbolo '<' fila:"+fila+" columna:"+Token);
                }
            }
        }else{
            sumaT();
            setError("se esperaba </personajes></escenario> fila:"+fila+" columna:"+Token);
        }
    }
    
    
    private String EscenarioNombre,EscenarioFondo,EscenarioAncho,EscenarioAlto;
    
    private void evaluaEscenario(String cad,int val){
        String tipo="";
        if(cad.length()>val){
            while(cad.charAt(val)!='='&&cad.length()-1>val){
                if(cad.charAt(val)!='\t'||cad.charAt(val)!=' '){
                    if(cad.charAt(val)=='\n'){
                        fila++;Token=0;
                    }else
                    {
                        tipo=tipo+cad.charAt(val)+"";
                    }
                }
                val++;
            }
            if(cad.length()>val){
                if(cad.charAt(val)=='='){
                    sumaT();
                    switch(tipo){
                        case "nombre":
                            setTokensList("nombre", fila, val);
                            sumaT();
                            setTokensList("=", fila, val);
                            
                            val++;
                            if(cad.length()>val){
                                String config="";
                                while(cad.length()-1>val&&cad.charAt(val)!=';'){
                                    config=config+cad.charAt(val);
                                    val++;
                                }
                                if(cad.charAt(val)==';'){
                                    sumaT();
                                    setTokensList(config, fila, val);
                                    //configuracion previa
                                    EscenarioNombre=config;
                                    ///HACER CON
                                }else{
                                    setError("falta ';' fila:"+fila+" columna:"+Token);
                                }
                            }else{
                                sumaT();
                                setError(tipo+"no tiene configuracion fila:"+fila+" columna:"+Token);
                            }
                            break;
                        case "fondo":
                            setTokensList("fondo", fila, val);
                            sumaT();
                            setTokensList("=", fila, val);
                            val++;
                            if(cad.length()>val){
                                String config="";
                                while(cad.length()-1>val&&cad.charAt(val)!=';'){
                                    config=config+cad.charAt(val);
                                    val++;
                                }
                                if(cad.charAt(val)==';'){
                                    sumaT();
                                    setTokensList(config, fila, val);
                                    //configuracion previa
                                    EscenarioFondo=config;
                                    ///HACER CON
                                }else{
                                    setError("falta ';' fila:"+fila+" columna:"+Token);
                                }
                            }else{
                                sumaT();
                                setError(tipo+"no tiene configuracion fila:"+fila+" columna:"+Token);
                            }
                            break;
                        case "ancho":
                            setTokensList("ancho", fila, val);
                            sumaT();
                            setTokensList("=", fila, val);
                            val++;
                            if(cad.length()>val){
                                String config="";
                                while(cad.length()-1>val&&cad.charAt(val)!=';'){
                                    config=config+cad.charAt(val);
                                    val++;
                                }
                                if(cad.charAt(val)==';'){
                                    sumaT();
                                    setTokensList(config, fila, val);
                                    EscenarioAncho=config;
                                }else{
                                    setError("falta ';' fila:"+fila+" columna:"+Token);
                                }
                            }else{
                                sumaT();
                                setError(tipo+"no tiene configuracion fila:"+fila+" columna:"+Token);
                            }
                            break;
                        case "alto":
                            setTokensList("alto", fila, val);
                            sumaT();
                            setTokensList("=", fila, val);
                            val++;
                            if(cad.length()>val){
                                String config="";
                                while(cad.length()-1>val&&cad.charAt(val)!=';'){
                                    config=config+cad.charAt(val);
                                    val++;
                                }
                                if(cad.charAt(val)==';'){
                                    sumaT();
                                    setTokensList(config, fila, val);
                                    //configuracion previa
                                    EscenarioAlto=config;
                                    ///HACER CON
                                }else{
                                    setError("falta ';' fila:"+fila+" columna:"+Token);
                                }
                            }else{
                                sumaT();
                                setError(tipo+"no tiene configuracion fila:"+fila+" columna:"+Token);
                            }
                            break;
                        default:
                            setError(tipo+" no es una cadena que se esperaba fila:"+fila+" columna:"+Token);
                            break;
                    }
                    val++;
                    if(cad.length()>val){
                        val++;
                        evaluaEscenario(cad, val);
                    }
                    
                }else{
                    sumaT();
                    setError("no se encontro el simbolo '=' fila:"+fila+" columna:"+Token);
                }
                
            }else{
                setError("falta configuracion dentro del escenario");
            }
        }else{
            //configuracion
        }
    }
    
    
    private void sumaT(){
        Token++;
        nTokens++;
    }
    
    private String retCadena(){
        String retorno=""; 
        if(cadena.charAt(pos)!='\t'||cadena.charAt(pos)!=' '){
            if(cadena.charAt(pos)=='\n'){
                fila++;Token=0;
            }else{
                retorno=cadena.charAt(pos)+"";
            }
        }
        pos++;
        return retorno;
    }
    
    
    
    private String cierraMayor(){
        String cad="";
        if(cadena.length()>pos){
            while(cadena.length()-1>pos && cadena.charAt(pos)!='>'){
                cad=cad+retCadena();
            }
        }
        return cad;
    }
    
    private void setTokensList(String cad, int fila, int col){
        String cadena = "Token"+nTokens+" Nombre:\""+cad+"\"\t\t\tfila:"+fila+"columna:"+Token;
        TokenList="".equals(TokenList)?cadena:TokenList+"\n"+cadena;
    }
    private void setError(String cad){
        Error=(Error.equals(""))?cad:Error+"\n"+cad;
    }
    public String getTokenList(){
        return TokenList;
    }
    public String getError(){
        return Error;
    }
    
    public ConfigEscenario configuracion(){
        return CONFIG;
    }
    
}
