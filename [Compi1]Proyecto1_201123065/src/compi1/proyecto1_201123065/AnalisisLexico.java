
package compi1.proyecto1_201123065;
import Nodos.Fondo;
import Nodos.Enemigo;
import Nodos.Diseno;
public class AnalisisLexico {
    Fondo f;
    Enemigo fig;
    Diseno dis;
    public AnalisisLexico(){
        f= new Fondo();
        fig=new Enemigo();
        dis=new Diseno();
    }
    private int fila,Token,nTokens,pos;
    private String TokenList,Errores,cadena;
    //catalogo para el fondo
    private String NombreFondo="",ImagenFondo="";
    //catalogo para la figura
    private String NombreEnemigo="",ImagenEnemigo="",DescripcionEnemigo="";
    
    int VidaEnemigo=0,DestruirEnemigo=0;
    //booleano para identificar si es heroe
    boolean heroe = false;
    
    public void inicio(String cadena){
        fila=Token=pos=0;
        fila++;
        TokenList=Errores="";
        this.cadena=cadena;
        try{
        instr();}
        catch(Exception e){
            setError("algun error no identificado se ha producido, favor revise su archivo");
        }
    }
    private void instr(){
        String error="";
        String cad="";
        while(cadena.length()>pos){
            if(cadena.charAt(pos)=='<'){
                cad=retornaCierre();
                if(cadena.charAt(pos)=='>'){
                    if(!error.equals("")){
                        setError(error+" no es una cadena valida");
                    }
                    if(cad.equals("<configuracion")){
                        sumaTokens();
                        setTokenList("<configuracion>", fila, Token);
                        iniciaConfig();
                    }
                    else{
                        setError("se esperaba <configuracion> no <"+cad+"> fila:"+fila+" columna:"+Token);
                    }
                    error="";
                    /*instrucciones*/
                }else if(cad.equals("configuracion")){
                    setError("falta el token '>' para configuracion");
                    pos++;
                }else{
                    sumaTokens();
                    setError(cad+" no es una cadena esperada fila:"+fila+" columna:"+Token);
                }
            }else {
                if(cadena.charAt(pos)!='\n'&&cadena.charAt(pos)!='\t'&&cadena.charAt(pos)!=' '){
                    error=error+cadena.charAt(pos);
                }
                pos++;
            }
        }
        if(!error.equals("")){
            setError("se esperaba <configuracion>  o vacio, no '"+error+"'");
            pos++;
        }
    }
    private void iniciaConfig(){
        String error="";
        String cad="";
        boolean ver=false;
        while(cadena.length()>pos-1&&!cad.equals("</configuracion")){
            if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                if(cadena.charAt(pos)=='\n'){
                    fila++;
                    Token=0;
                    pos++;
                }else if(cadena.charAt(pos)=='<'){
                    cad=retornaCierre();
                    if(cadena.charAt(pos)=='>'){
                        sumaTokens();
                        if(!error.equals("")){
                            setError(error+" no es una cadena valida");
                        }
                        switch (cad){
                            case "</configuracion":
                                setTokenList("</configuracion>", fila, Token);
                                ver=true;
                                break;
                            case "<fondo":
                                setTokenList("<fondo>", fila, Token);
                                CierraFondo();
                                break;
                            case "<figura":
                                setTokenList("<figura>", fila, Token);
                                CierraFigura();
                                break;
                            case "<diseño":
                                setTokenList("<diseño>", fila, Token);
                                CierraDiseno();
                                break;
                                default:
                                    setError("'"+cad+"' no se esperaba, fila:"+fila+" columna:"+Token);
                                break;
                        }
                        error="";
                        /*instrucciones*/
                    }else if(cad.equals("/configuracion")){
                        setError("falta el token '>' para /configuracion");
                        pos++;
                    }else{
                        Token++;
                        setError(cad+" no es una cadena esperada fila:"+fila+" columna:"+Token);
                        pos++;
                    }
                }else {
                    error=error+cadena.charAt(pos);
                    pos++;
                }
            }else{
                pos++;
            }
        }
        if(!error.equals("")){
            sumaTokens();
            setError("Token no reconocido: "+error);
        }
        if(ver==false){
            setError("falta la expresion </configuracion>");
        }
    } 
    
    /*Configuracion del fondo*/
    private void CierraFondo(){
        String cierre="";
        String error="";
        String cad= retornaMayor();
        cierre=retornaCierre();
        if(cadena.charAt(pos)=='>'){
            if(cierre.equals("</fondo")){
                if(cad.equals("")){
                    setError("la etiqueta <fondo></fondo> no tiene funcion");
                }else{
                    Fondo(cad,0);
                    
                }
                Token++;
                nTokens++;
                setTokenList("</fondo>", fila, Token);
                pos++;
            }else{
                Token++;
                pos++;
                setError(cierre+" no es una cadena esperada fila:"+fila+" columna:"+Token);
                
            }
        }else{
            setError("no se encontro la cadena '></configuracion>'");
        }
    } 
    private void Fondo(String cad,int paso){
        String subC="";
        String error="";
        if(cad.length()>paso){
            while(cad.charAt(paso)!='{'&&cad.length()-1>paso){
                error=error+cad.charAt(paso);
                paso++;
            }
            if(cad.length()<=paso){
                 Token++;
                nTokens++;
                setError("no se esperaba "+error);
                error="";
                paso++;
            }else{
               if(cad.charAt(paso)=='{'){
                    if(!error.equals("")){
                        Token++;
                        nTokens++;
                        setError(error+" no se esperaba antes del simbolo '{'");
                        error="";
                    }
                    Token++;
                    nTokens++;
                    paso++;
                    setTokenList("{", fila, Token);
                    if(paso<cad.length()){
                        while(cad.charAt(paso)!='}'&&cad.length()-1>paso){
                            subC=subC+cad.charAt(paso);
                            paso++;
                        }
                        if(cad.charAt(paso)=='}'){
                            ConfiguraFondo(subC, 0);
                            paso++;
                            Token++;
                            nTokens++;
                            setTokenList("}", fila, Token);
                            if(NombreFondo.equals("")||ImagenFondo.equals("")){
                                setError("falta su configuracion");
                            }
                            else{
                                f.Agregar(NombreFondo, ImagenFondo);
                            }
                            NombreFondo=ImagenFondo="";
                            if(cad.length()>paso){
                                if(cad.charAt(paso)==','){
                                    Token++;
                                    nTokens++;
                                    setTokenList(",", fila, Token);
                                    Fondo(cad,paso+1);
                                }
                                else{
                                    Token++;
                                    nTokens++;
                                    setError("falta ',' fila:"+fila+" columna:"+Token);
                                }
                            }
                        }else{
                            Token++;
                            setError(subC+" nunca se cerró, falta simbolo '}' fila:"+fila+" columna:"+Token);
                        }
                    }else{
                        Token++;
                        setError(subC+" nunca se cerró, falta simbolo '}' fila:"+fila+" columna:"+Token);
                    }
                }else{
                   nTokens++;
                   Token++;
                    setError("falta simbolo '{' fila:"+fila+" columna:"+Token);
                }
            }
        }else{
            nTokens++;
            Token++;
            setError("falta el simbolo '{' en fila:"+fila+" columna:"+Token );
        }
        
    }
    private void ConfiguraFondo(String cad,int paso){
        String tipo="",nombre="",imagen="";
        if(cad.length()>paso){
            nTokens++;
            Token++;
            while(cad.length()-1>paso&&cad.charAt(paso)!='='){
                tipo=tipo+cad.charAt(paso);
                paso++;
            }
            if(cad.charAt(paso)=='='){
                switch(tipo){
                    case "nombre":
                        setTokenList("nombre", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no ha escrito ningun nombre fila:"+fila+" columna"+Token);
                                }else{
                                    NombreFondo=nombre;
                                    setTokenList(nombre, fila, Token);
                                    nTokens++;
                                    Token++;    
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    if(cad.length()>paso){
                                        ConfiguraFondo(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el nombre de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                    case "imagen":
                       setTokenList("imagen", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                imagen=imagen+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            int sPaso=0;
                            String nError="";
                            if(cad.charAt(paso)==';'){
                                if(imagen.equals("")){
                                    setError("no ha colocado ningura ruta de la imagen; fila:"+fila+" columna"+Token);
                                }else{
                                    String ruta="";
                                    if(imagen.length()>sPaso){
                                        while(imagen.length()-1>sPaso&&imagen.charAt(sPaso)!='\"'){
                                            nError=nError+imagen.charAt(sPaso);
                                            sPaso++;
                                        }
                                        if(!nError.equals("")){
                                            Token++;
                                            nTokens++;
                                            setError(" parece que falta el simbolo \" antes de la ruta");
                                            nError="";
                                        }else{
                                            sPaso++;
                                            if(imagen.length()>sPaso){
                                                while(imagen.length()-1>sPaso&&imagen.charAt(sPaso)!='"'){
                                                    ruta= ruta+imagen.charAt(sPaso);
                                                    sPaso++;
                                                }
                                                    if(ruta.equals("")){
                                                        setError("la imagen parece que no contiene ninguna ruta");
                                                    }else{
                                                        if(imagen.length()>sPaso){
                                                            if(imagen.charAt(sPaso)=='\"'){
                                                                Token++;
                                                                nTokens++;
                                                                ImagenFondo=ruta;
                                                                setTokenList(ruta, fila, Token);
                                                                nTokens++;
                                                                Token++;    
                                                                setTokenList(";", fila, Token);
                                                                paso++;
                                                                if(cad.length()>paso){
                                                                    ConfiguraFondo(cad, paso);
                                                                }
                                                            }else{
                                                                nTokens++;
                                                                Token++;
                                                                setError("falta cerrar con el simbolo \" fila:"+fila+" columna:"+Token);
                                                            }
                                                        }else{
                                                            nTokens++;
                                                            Token++;
                                                            setError("falta cerrar con el simbolo \" fila:"+fila+" columna:"+Token);
                                                        }
                                                    }
                                                }else{
                                                    setError("falta el simbolo \" ");
                                                }
                                        }
                                        
                                    }else{
                                            setError(" la imagen parece que no contiene ninguna ruta");
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el nombre de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                    default:
                        setError(tipo+" no es una cadena valida fila:"+fila+" columna:"+Token);
                        System.out.println("sip, no se que carajos, pero aca da clavo");
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        break;
                }
                
            }else{
                nTokens++;
                Token++;
                setError("se esperaba el simbolo '=' fila:"+fila+" columna:"+Token);
            }
        }else{
            
        }   
    }     
    
    public Diseno RetornaD(){
        return dis;
    }
    public Enemigo RetornaE(){
        return fig;
    }
    public Fondo RetornaF(){
        return f;
    }
    
    
   /*Configuracion de la figura*/
    private void CierraFigura(){
        String cierre="";
        String error="";
        String cad= retornaMayor();
        cierre=retornaCierre();
        if(cadena.charAt(pos)=='>'){
            if(cierre.equals("</figura")){
                if(cad.equals("")){
                    setError("la etiqueta <figura></figura> no tiene funcion");
                }else{
                    /*FIGURA!!!!*/
                    Figura(cad,0);
                }
                Token++;
                nTokens++;
                setTokenList("</figura>", fila, Token);
                pos++;
            }else{
                Token++;
                pos++;
                setError(cierre+" no es una cadena esperada fila:"+fila+" columna:"+Token);
                
            }
        }else{
            setError("no se encontro la cadena '></configuracion>'");
        }
    } 
    private void Figura(String cad, int paso){
         String subC="";
        String error="";
        if(cad.length()>paso){
            while(cad.charAt(paso)!='{'&&cad.length()-1>paso){
                error=error+cad.charAt(paso);
                paso++;
            }
            if(cad.length()<=paso){
                 Token++;
                nTokens++;
                setError("no se esperaba "+error);
                error="";
                paso++;
            }else{
               if(cad.charAt(paso)=='{'){
                    if(!error.equals("")){
                        Token++;
                        nTokens++;
                        setError(error+" no se esperaba antes del simbolo '{'");
                        error="";
                    }
                    Token++;
                    nTokens++;
                    paso++;
                    setTokenList("{", fila, Token);
                    if(paso<cad.length()){
                        while(cad.charAt(paso)!='}'&&cad.length()-1>paso){
                            subC=subC+cad.charAt(paso);
                            paso++;
                        }
                        if(cad.charAt(paso)=='}'){
                            ConfiguraFigura(subC, 0);
                            paso++;
                            Token++;
                            nTokens++;
                            setTokenList("}", fila, Token);
                            if(NombreEnemigo.equals("")||ImagenEnemigo.equals("")){
                                setError("falta la configuracion de la figura");
                            }
                            else{
                                if(heroe==true){
                                    fig.Agregar(true,NombreEnemigo, VidaEnemigo, ImagenEnemigo, 0,DescripcionEnemigo);
                                   
                                }
                                else{
                                    fig.Agregar(false,NombreEnemigo, VidaEnemigo, ImagenEnemigo, DestruirEnemigo, DescripcionEnemigo);
                                }
                                NombreEnemigo=ImagenEnemigo=DescripcionEnemigo="";
                                DestruirEnemigo=VidaEnemigo=0;
                            }
                            if(cad.length()>paso){
                                if(cad.charAt(paso)==','){
                                    Token++;
                                    nTokens++;
                                    setTokenList(",", fila, Token);
                                    Figura(cad,paso+1);
                                }
                                else{
                                    Token++;
                                    nTokens++;
                                    setError("falta ',' fila:"+fila+" columna:"+Token);
                                }
                            }
                        }else{
                            Token++;
                            setError(subC+" nunca se cerró, falta simbolo '}' fila:"+fila+" columna:"+Token);
                        }
                    }else{
                        Token++;
                        setError(subC+" nunca se cerró, falta simbolo '}' fila:"+fila+" columna:"+Token);
                    }
                }else{
                   nTokens++;
                   Token++;
                    setError("falta simbolo '{' fila:"+fila+" columna:"+Token);
                }
            }
        }else{
            nTokens++;
            Token++;
            setError("falta el simbolo '{' en fila:"+fila+" columna:"+Token );
        }
    }
    private void ConfiguraFigura(String cad, int paso){
        String tipo="",nombre="",imagen="",descripcion="";
        int vida=0, destruir=0;
        if(cad.length()>paso){
            nTokens++;
            Token++;
            while(cad.length()-1>paso&&cad.charAt(paso)!='='){
                tipo=tipo+cad.charAt(paso);
                paso++;
            }
            if(cad.charAt(paso)=='='){
                switch(tipo){
                    case "nombre":
                        setTokenList("nombre", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no ha escrito ningun nombre fila:"+fila+" columna"+Token);
                                }else{
                                    NombreEnemigo=nombre;
                                    setTokenList(nombre, fila, Token);
                                    nTokens++;
                                    Token++;    
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    if(cad.length()>paso){
                                        ConfiguraFigura(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el nombre de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                    case "imagen":
                       setTokenList("imagen", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                imagen=imagen+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            int sPaso=0;
                            String nError="";
                            if(cad.charAt(paso)==';'){
                                if(imagen.equals("")){
                                    setError("no ha colocado ningura ruta de la imagen; fila:"+fila+" columna"+Token);
                                }else{
                                    String ruta="";
                                    if(imagen.length()>sPaso){
                                        while(imagen.length()-1>sPaso&&imagen.charAt(sPaso)!='\"'){
                                            nError=nError+imagen.charAt(sPaso);
                                            sPaso++;
                                        }
                                        if(!nError.equals("")){
                                            Token++;
                                            nTokens++;
                                            setError(" parece que falta el simbolo \" antes de la ruta");
                                            nError="";
                                        }else{
                                            sPaso++;
                                            if(imagen.length()>sPaso){
                                                while(imagen.length()-1>sPaso&&imagen.charAt(sPaso)!='"'){
                                                    ruta= ruta+imagen.charAt(sPaso);
                                                    sPaso++;
                                                }
                                                    if(ruta.equals("")){
                                                        setError("la imagen parece que no contiene ninguna ruta");
                                                    }else{
                                                        if(imagen.length()>sPaso){
                                                            if(imagen.charAt(sPaso)=='\"'){
                                                                Token++;
                                                                nTokens++;
                                                                ImagenEnemigo=ruta;
                                                                setTokenList(ruta, fila, Token);
                                                                nTokens++;
                                                                Token++;    
                                                                setTokenList(";", fila, Token);
                                                                paso++;
                                                                if(cad.length()>paso){
                                                                    ConfiguraFigura(cad, paso);
                                                                }
                                                            }else{
                                                                nTokens++;
                                                                Token++;
                                                                setError("falta cerrar con el simbolo \" fila:"+fila+" columna:"+Token);
                                                            }
                                                        }else{
                                                            nTokens++;
                                                            Token++;
                                                            setError("falta cerrar con el simbolo \" fila:"+fila+" columna:"+Token);
                                                        }
                                                    }
                                                }else{
                                                    setError("falta el simbolo \" ");
                                                }
                                        }
                                        
                                    }else{
                                            setError(" la imagen parece que no contiene ninguna ruta");
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el nombre de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                        
                        
                        
                    case "vida":
                        setTokenList("vida", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no hay registro de vida fila:"+fila+" columna"+Token);
                                }else{
                                    VidaEnemigo=Integer.parseInt(nombre);
                                    setTokenList(nombre, fila, Token);
                                    nTokens++;
                                    Token++;    
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    if(cad.length()>paso){
                                        ConfiguraFigura(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el valor de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                        
                    case "tipo":
                        setTokenList("tipo", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no se sabe de que tipo es fila:"+fila+" columna"+Token);
                                }else{
                                    setTokenList(nombre, fila, Token);
                                    nTokens++;
                                    Token++; 
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    
                                    //vital que lo recuerde, el valor de heroe cambia a true
                                    if(nombre.contains("heroe")){
                                        heroe=true;
                                    }else if(nombre.contains("enemigo")){
                                        heroe=false;
                                    }else{
                                        nTokens++;
                                        Token++;
                                        setError(nombre+" no reconocido fila:"+fila+" columna:"+Token);
                                    }
                                    if(cad.length()>paso){
                                        ConfiguraFigura(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el tipo de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                        
                    case "destruir":
                        setTokenList("destruir", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no hay registro de valor de destruccion fila:"+fila+" columna"+Token);
                                }else{
                                    DestruirEnemigo=Integer.parseInt(nombre);
                                    setTokenList(nombre, fila, Token);
                                    nTokens++;
                                    Token++;    
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    if(cad.length()>paso){
                                        ConfiguraFigura(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el valor de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                        
                    case "descripcion":
                        setTokenList("descripcion", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                imagen=imagen+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            int sPaso=0;
                            String nError="";
                            if(cad.charAt(paso)==';'){
                                if(imagen.equals("")){
                                    setError("no ha colocado ningura descripcion; fila:"+fila+" columna"+Token);
                                }else{
                                    String ruta="";
                                    if(imagen.length()>sPaso){
                                        while(imagen.length()-1>sPaso&&imagen.charAt(sPaso)!='\"'){
                                            nError=nError+imagen.charAt(sPaso);
                                            sPaso++;
                                        }
                                        if(!nError.equals("")){
                                            Token++;
                                            nTokens++;
                                            setError(" parece que falta el simbolo \" antes de la ruta");
                                            nError="";
                                        }else{
                                            sPaso++;
                                            if(imagen.length()>sPaso){
                                                while(imagen.length()-1>sPaso&&imagen.charAt(sPaso)!='"'){
                                                    ruta= ruta+imagen.charAt(sPaso);
                                                    sPaso++;
                                                }
                                                    if(ruta.equals("")){
                                                        setError("la imagen parece que no contiene ninguna descripcion");
                                                    }else{
                                                        if(imagen.length()>sPaso){
                                                            if(imagen.charAt(sPaso)=='\"'){
                                                                Token++;
                                                                nTokens++;
                                                                DescripcionEnemigo=ruta;
                                                                setTokenList(ruta, fila, Token);
                                                                nTokens++;
                                                                Token++;    
                                                                setTokenList(";", fila, Token);
                                                                paso++;
                                                                if(cad.length()>paso){
                                                                    ConfiguraFigura(cad, paso);
                                                                }
                                                            }else{
                                                                nTokens++;
                                                                Token++;
                                                                setError("falta cerrar con el simbolo \" fila:"+fila+" columna:"+Token);
                                                            }
                                                        }else{
                                                            nTokens++;
                                                            Token++;
                                                            setError("falta cerrar con el simbolo \" fila:"+fila+" columna:"+Token);
                                                        }
                                                    }
                                                }else{
                                                    setError("falta el simbolo \" ");
                                                }
                                        }
                                        
                                    }else{
                                            setError(" la imagen parece que no contiene ninguna ruta");
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el nombre de la cadena fila:"+fila+" columna:"+Token);
                        }
                        
                        break;
                        
                    default:
                        setError(tipo+" no es una cadena valida fila:"+fila+" columna:"+Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        break;
                }
                
            }else{
                nTokens++;
                Token++;
                setError("se esperaba el simbolo '=' fila:"+fila+" columna:"+Token);
            }
        }else{
            
        }   
        
    }
    
    private String NombreDis="",ImagenDis="",tipoDis="";
    private int PuntosDis=0;
    /*Configura el diseño*/
    private void CierraDiseno(){
        String cierre="";
        String error="";
        String cad= retornaMayor();
        cierre=retornaCierre();
        if(cadena.charAt(pos)=='>'){
            if(cierre.equals("</diseño")){
                if(cad.equals("")){
                    setError("la etiqueta <diseño></diseño> no tiene funcion");
                }else{
                    /*DISEÑO!!!!*/
                    Diseno(cad,0);
                }
                Token++;
                nTokens++;
                setTokenList("</diseño>", fila, Token);
                pos++;
            }else{
                Token++;
                pos++;
                System.out.println(cierre);
                setError(cierre+" no es una cadena esperada fila:"+fila+" columna:"+Token);
                
            }
        }else{
            setError("no se encontro la cadena '></configuracion>'");
        }
    }
    public void Diseno(String cad, int paso){
         String subC="";
        String error="";
        if(cad.length()>paso){
            while(cad.charAt(paso)!='{'&&cad.length()-1>paso){
                error=error+cad.charAt(paso);
                paso++;
            }
            if(cad.length()<=paso){
                 Token++;
                nTokens++;
                setError("no se esperaba "+error);
                error="";
                paso++;
            }else{
               if(cad.charAt(paso)=='{'){
                    if(!error.equals("")){
                        Token++;
                        nTokens++;
                        setError(error+" no se esperaba antes del simbolo '{'");
                        error="";
                    }
                    Token++;
                    nTokens++;
                    paso++;
                    setTokenList("{", fila, Token);
                    if(paso<cad.length()){
                        while(cad.charAt(paso)!='}'&&cad.length()-1>paso){
                            subC=subC+cad.charAt(paso);
                            paso++;
                        }
                        if(cad.charAt(paso)=='}'){
                            ConfiguraDiseno(subC, 0);
                            paso++;
                            Token++;
                            nTokens++;
                            setTokenList("}", fila, Token);
                            if(NombreDis.equals("")||ImagenDis.equals("")||tipoDis.equals("")){
                                setError("falta la configuracion del diseño");
                            }
                            else{
                                
                                dis.Agregar(tipoDis, NombreDis, ImagenDis,PuntosDis);
                            }
                            tipoDis=NombreDis=ImagenDis="";
                            PuntosDis=0;
                            if(cad.length()>paso){
                                if(cad.charAt(paso)==','){
                                    Token++;
                                    nTokens++;
                                    setTokenList(",", fila, Token);
                                    Diseno(cad,paso+1);
                                }
                                else{
                                    Token++;
                                    nTokens++;
                                    setError("falta ',' fila:"+fila+" columna:"+Token);
                                }
                            }
                        }else{
                            Token++;
                            setError(subC+" nunca se cerró, falta simbolo '}' fila:"+fila+" columna:"+Token);
                        }
                    }else{
                        Token++;
                        setError(subC+" nunca se cerró, falta simbolo '}' fila:"+fila+" columna:"+Token);
                    }
                }else{
                   nTokens++;
                   Token++;
                    setError("falta simbolo '{' fila:"+fila+" columna:"+Token);
                }
            }
        }else{
            nTokens++;
            Token++;
            setError("falta el simbolo '{' en fila:"+fila+" columna:"+Token );
        }
    }
    public void ConfiguraDiseno(String cad,int paso){
        String tipo="",nombre="",imagen="";
        if(cad.length()>paso){
            nTokens++;
            Token++;
            while(cad.length()-1>paso&&cad.charAt(paso)!='='){
                tipo=tipo+cad.charAt(paso);
                paso++;
            }
            if(cad.charAt(paso)=='='){
                switch(tipo){
                    case "nombre":
                        setTokenList("nombre", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no ha escrito ningun nombre fila:"+fila+" columna"+Token);
                                }else{
                                    NombreDis=nombre;
                                    setTokenList(nombre, fila, Token);
                                    nTokens++;
                                    Token++;    
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    if(cad.length()>paso){
                                        ConfiguraDiseno(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el nombre de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                    case "imagen":
                        setTokenList("imagen", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                imagen=imagen+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            int sPaso=0;
                            String nError="";
                            if(cad.charAt(paso)==';'){
                                if(imagen.equals("")){
                                    setError("no ha colocado ningura ruta de la imagen; fila:"+fila+" columna"+Token);
                                }else{
                                    String ruta="";
                                    if(imagen.length()>sPaso){
                                        while(imagen.length()-1>sPaso&&imagen.charAt(sPaso)!='\"'){
                                            nError=nError+imagen.charAt(sPaso);
                                            sPaso++;
                                        }
                                        if(!nError.equals("")){
                                            Token++;
                                            nTokens++;
                                            setError(" parece que falta el simbolo \" antes de la ruta");
                                            nError="";
                                        }else{
                                            sPaso++;
                                            if(imagen.length()>sPaso){
                                                while(imagen.length()-1>sPaso&&imagen.charAt(sPaso)!='"'){
                                                    ruta= ruta+imagen.charAt(sPaso);
                                                    sPaso++;
                                                }
                                                    if(ruta.equals("")){
                                                        setError("la imagen parece que no contiene ninguna ruta");
                                                    }else{
                                                        if(imagen.length()>sPaso){
                                                            if(imagen.charAt(sPaso)=='\"'){
                                                                Token++;
                                                                nTokens++;
                                                                ImagenDis=ruta;
                                                                setTokenList(ruta, fila, Token);
                                                                nTokens++;
                                                                Token++;    
                                                                setTokenList(";", fila, Token);
                                                                paso++;
                                                                if(cad.length()>paso){
                                                                    ConfiguraDiseno(cad, paso);
                                                                }
                                                            }else{
                                                                nTokens++;
                                                                Token++;
                                                                setError("falta cerrar con el simbolo \" fila:"+fila+" columna:"+Token);
                                                            }
                                                        }else{
                                                            nTokens++;
                                                            Token++;
                                                            setError("falta cerrar con el simbolo \" fila:"+fila+" columna:"+Token);
                                                        }
                                                    }
                                                }else{
                                                    setError("falta el simbolo \" ");
                                                }
                                        }
                                        
                                    }else{
                                            setError(" la imagen parece que no contiene ninguna ruta");
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el nombre de la cadena fila:"+fila+" columna:"+Token);
                        }
                        break;
                    case "tipo":
                        setTokenList("tipo", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no ha asignado ningun tipo fila:"+fila+" columna"+Token);
                                }else{
                                    tipoDis=nombre;
                                    setTokenList(nombre, fila, Token);
                                    nTokens++;
                                    Token++;    
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    if(cad.length()>paso){
                                        ConfiguraDiseno(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el tipo fila:"+fila+" columna:"+Token);
                        }
                        break;
                    case "destruir":
                        setTokenList("destruir", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no ha asignado ningun valor fila:"+fila+" columna"+Token);
                                }else{
                                    try{
                                        PuntosDis=Integer.parseInt(nombre);
                                    setTokenList(nombre, fila, Token);
                                        
                                    }catch(Exception e){
                                        setError("el valor de la destruccion no parece ser un entero "+nombre);
                                    }
                                    nTokens++;
                                    Token++;    
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    if(cad.length()>paso){
                                        ConfiguraDiseno(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el valor fila:"+fila+" columna:"+Token);
                        }
                        break;
                    case "creditos":
                        setTokenList("creaditos", fila, Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        paso++;
                        if(cad.length()>paso){
                            while(cad.length()-1>paso&&cad.charAt(paso)!=';'){
                                nombre=nombre+cad.charAt(paso);
                                paso++;
                            }
                            nTokens++;
                            Token++;
                            if(cad.charAt(paso)==';'){
                                if(nombre.equals("")){
                                    setError("no ha asignado ningun credito fila:"+fila+" columna"+Token);
                                }else{
                                    try{
                                    PuntosDis=Integer.parseInt(nombre);
                                    setTokenList(nombre, fila, Token);
                                    
                                    }catch(Exception e){
                                        setError("el valor de los creditos parece que no son enteros "+nombre);
                                    }
                                    nTokens++;
                                    Token++;    
                                    setTokenList(";", fila, Token);
                                    paso++;
                                    if(cad.length()>paso){
                                        ConfiguraDiseno(cad, paso);
                                    }
                                }
                            }else{
                                nTokens++;
                                Token++;
                                setError("falta el simbolo ';' fila:"+fila+" columna:"+Token);
                            }
                        }else{
                            nTokens++;
                            Token++;
                            setError("falta colocar el tipo fila:"+fila+" columna:"+Token);
                        }
                        break;
                    default:
                        setError(tipo+" no es una cadena valida fila:"+fila+" columna:"+Token);
                        nTokens++;
                        Token++;
                        setTokenList("=", fila, Token);
                        break;
                }
                
            }else{
                nTokens++;
                Token++;
                setError("se esperaba el simbolo '=' fila:"+fila+" columna:"+Token);
            }
        }else{
            
        }   
        
    }
    
    
    
    
    
    String nombreHeroe,imagenHeroe,descripcionHeroe;
    int VHeroe;
    private void Heroe(String nombre, int vida,String imagen,String descripcion){
        nombreHeroe=nombre;
        imagenHeroe=imagen;
        vida=VHeroe;
        descripcionHeroe=descripcion;
    }
    
    
    
    private void sumaTokens(){
        pos++;
        Token++;
        nTokens++;

     }
     
    private String retornaMayor(){
        String cad="";
        while(cadena.charAt(pos)!='<'&&cadena.length()-1>pos){
            if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                if(cadena.charAt(pos)!='\n'){
                    cad=cad+cadena.charAt(pos);
                }else{
                    fila++;
                    Token=0;
                }
            }
            pos++;
        }
        return cad;
    }
    private String retornaCierre(){
        String cad="";
        while(cadena.length()-1>pos && cadena.charAt(pos)!='>'){
            if(cadena.charAt(pos)!=' '&&cadena.charAt(pos)!='\t'){
                if(cadena.charAt(pos)=='\n'){
                    fila++;
                    Token=0;
                }else{
                    cad=cad+cadena.charAt(pos);
                }
            }
            pos++;
        }
        return cad;
    }
    
    
    
    private void setTokenList(String cad,int fila, int token){
        String cadena = "Token"+nTokens+" Nombre:\""+cad+" \"\t\t\tfila:"+fila+"columna:"+token;
        TokenList="".equals(TokenList)?cadena:TokenList+"\n"+cadena;
    }
    public String getTokens(){
        return TokenList;
    }
    private void setError(String cad){
        Errores=Errores.equals("")?cad:Errores+"\n"+cad;
    }
    public String getError(){
        return Errores;
    }
    
    
}
