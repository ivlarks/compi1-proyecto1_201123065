
package JuegoConfig;


public class NodoPersonaje {
    String nombre;
    int alto,ancho;
    Boolean tipo;
    NodoPersonaje sig,ant;
    public NodoPersonaje(boolean tipo,String nombre,int alto,int ancho)
    {
        this(tipo,nombre,alto,ancho,null,null);
    }    
    public NodoPersonaje(Boolean tipo,String nombre,int alto, int ancho,NodoPersonaje sig,NodoPersonaje ant){
        this.tipo=tipo;
        this.nombre=nombre;
        this.alto=alto;
        this.ancho=ancho;
        this.sig=sig;
        this.ant=ant;
    }
}
