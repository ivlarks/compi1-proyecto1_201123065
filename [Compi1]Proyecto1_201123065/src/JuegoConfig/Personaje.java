
package JuegoConfig;

public class Personaje {
    NodoPersonaje inicio,fin,aux;
    
    public Personaje(){
        inicio=fin=null;
    }
    
    public void ingresa(Boolean tipo,String nombre, int alto,int ancho){
        if(inicio==null){
            inicio=fin= new NodoPersonaje(tipo,nombre, alto, ancho);
        }else{
            aux=fin;
            fin= new NodoPersonaje(tipo,nombre, alto, ancho,aux,null);
            aux.sig=fin;
            fin.sig=null;
        }
    }
    public String ret(){
        aux=inicio;
        String cad="";
        while(aux!=null){
            cad=cad+"<>"+aux.nombre+"<>";
            aux=aux.sig;
        }
        return cad;
    }
}
