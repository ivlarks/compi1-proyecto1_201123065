
package JuegoConfig;

public class ConfigEscenario {
    NodoEscenario inicio,fin,aux;
    public ConfigEscenario(){
        inicio=fin=null;
    }
    private Boolean estaVacia(){
        return (inicio==null);
    }
    public void Agregar(String nombre, String fondo, int ancho, int alto, Personaje personajes,
            Pared paredes,Suelo suelo,Extras extra,String meta, int mx, int my){
        if(!estaVacia()){
            aux=fin;
            fin= new NodoEscenario(nombre, fondo, ancho, alto, personajes, paredes, suelo, extra, meta, mx, my, null, fin);
            aux.siguiente=fin;
            fin.siguiente=null;
        }else{
            inicio=fin=aux=new NodoEscenario(nombre, fondo, ancho, alto, personajes, paredes, suelo, extra, meta, mx, my);
        }
    }
    
    public String retornaNodos(){
        String cad = "";
        aux=inicio;
        while(aux!=null){
            String Scad="("+aux.nombre
                    +","+aux.fondo+","
                    +aux.ancho+","
                    +aux.alto+","
                    +aux.personaje.ret()+","
                    +aux.pared.retornaNodos()+","
                    +aux.suelo.retornaNodos()+","
                    +aux.extra.retornaNodos()+","
                    +aux.meta+","+aux.mx+","+aux.my;
            System.out.println(Scad);
            cad=(cad.equals(""))?Scad:cad+"\n"+Scad;
            aux=aux.siguiente;
        }
        return cad;
    }
}
