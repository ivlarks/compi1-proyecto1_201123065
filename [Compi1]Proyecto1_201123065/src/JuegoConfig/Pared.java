/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JuegoConfig;

/**
 *
 * @author marcosmayen
 */
public class Pared {
    NodoPared inicio,fin,aux;
    public Pared(){
        inicio=fin=null;
    }
    private Boolean estaVacia(){
        return (inicio==null);
    }
    public void Agregar(String nombre, int ix,int fx,int iy,int fy){
        if(!estaVacia()){
            aux=fin;
            fin= new NodoPared(nombre, ix, fx, iy, fy, fin, null);
            aux.siguiente=fin;
            fin.siguiente=null;
        }else{
            inicio=fin=aux=new NodoPared(nombre, ix, fx, iy, fy);
        }
    }
    
    public String retornaNodos(){
        String cad = "";
        aux=inicio;
        while(aux!=null){
            String Scad="("+aux.nombre+","+aux.ix+","+aux.fx+","+aux.iy+","+aux.fy+")";
            cad=(cad.equals(""))?Scad:cad+"\n"+Scad;
            aux=aux.siguiente;
        }
        return cad;
    }
    
    
}
