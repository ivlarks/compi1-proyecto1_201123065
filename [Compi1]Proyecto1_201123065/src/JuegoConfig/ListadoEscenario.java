
package JuegoConfig;


public class ListadoEscenario {
    
    private String error,Token;
    private String cadena;
    private int pos,fila,columna;
    public void init(String cadena){
        this.cadena=cadena;
        pos=fila=columna=0;
        inicia();
    }
    
    private void inicia(){
        String e="";
        if(cadena.length()>pos){
            while(cadena.charAt(pos)!='<'&&cadena.length()-1>pos){
                if(cadena.charAt(pos)!=' '&& cadena.charAt(pos)!='\t'){
                    if(cadena.charAt(pos)=='\n'){
                        fila++;columna=0;
                    }else{
                        e=e+cadena.charAt(pos);
                    }
                }
                pos++;
            }
            if(!e.equals("")){
                setError("no se esperaba "+e, fila, columna);
            }
            if(cadena.charAt(pos)=='<'){
                String esena="";
                pos++;
                if(cadena.length()>pos){
                    while(cadena.length()-1>pos&&cadena.charAt(pos)!='>'){
                        if(cadena.charAt(pos)!=' '&& cadena.charAt(pos)!='\t'){
                            if(cadena.charAt(pos)=='\n'){
                                fila++;columna=0;
                            }else{
                                esena=esena+cadena.charAt(pos);
                            }
                        }
                        pos++;
                    }
                    if(cadena.charAt(pos)=='>'){
                        pos++;
                        if(esena.equals("escenarios")){
                            if(cadena.length()>pos){
                                cierraEscenario();
                            }else{setError("falta configuracion de los escenarios", fila, columna);}
                        }else{setError("se esperaba escenarios no '"+esena+"'", fila, columna);}
                    }else{setError(" se esperaba '>' ", fila, columna);}
                }else{setError("se esperaba <escenarios>", fila, columna);}
            }
        }
    }
    private void cierraEscenario(){
        String err="";
        if(cadena.length()>pos){
            while(cadena.length()-1>pos&&cadena.charAt(pos)!='<'){
                if(cadena.charAt(pos)!=' '&& cadena.charAt(pos)!='\t'){
                    if(cadena.charAt(pos)=='\n'){
                        fila++;columna=0;
                    }else{
                        err=err+cadena.charAt(pos);
                    }
                }
                pos++;
            }
            if(!err.equals("")){setError("no se esperaba"+err, fila, columna);}
            if(cadena.charAt(pos)=='<'){
                String confi="";
                while(cadena.length()-1>pos&&cadena.charAt(pos)!='>'){
                    if(cadena.charAt(pos)!=' '&& cadena.charAt(pos)!='\t'){
                        if(cadena.charAt(pos)=='\n'){
                            fila++;columna=0;
                        }else{
                            confi=confi+cadena.charAt(pos);
                        }
                    }
                    pos++;
                }
                if(cadena.charAt(pos)=='>'){
                    pos++;
                    if(!confi.equals("/escenarios")){
                        configuraOrden(confi);
                    }
                    
                }else{setError("se esperaba '>'", fila, columna);}
            }else{setError("se esperaba '<' ", fila, columna);}
        }
    }
    
    private String NOMBRE,ORDEN;
    private void configuraOrden(String confi){
        int val=0,orden=0;
        String esc="",nombre="";
        if(confi.length()>val){
            while(confi.length()-1>val&&confi.charAt(val)!=' '){
                if(confi.charAt(val)!=' '&& confi.charAt(val)!='\t'){
                    if(confi.charAt(val)=='\n'){
                        fila++;columna=0;
                    }else{
                        esc=esc+confi.charAt(val);
                    }
                }
                val++;
            }
            if(confi.charAt(val)==' '){
                val++;
                if(esc.equals("escenario")){
                    do{
                        if(confi.length()>val){
                            while(confi.length()-1>val&&confi.charAt(val)!='='){
                                if(confi.charAt(val)!=' '&& confi.charAt(val)!='\t'){
                                    if(confi.charAt(val)=='\n'){
                                        fila++;columna=0;
                                    }else{
                                        nombre=nombre+confi.charAt(val);
                                    }
                                }
                                val++;
                            }
                            if(confi.charAt(val)=='='){
                                val++;
                                switch(nombre){
                                    case "nombre":
                                        nombre="";
                                        if(confi.length()>val){
                                            while(confi.length()-1>val&&confi.charAt(val)!=';'){
                                                if(confi.charAt(val)!=' '&& confi.charAt(val)!='\t'){
                                                    if(confi.charAt(val)=='\n'){
                                                        fila++;columna=0;
                                                    }else{
                                                        nombre=nombre+confi.charAt(val);
                                                    }
                                                }
                                                val++;
                                            }
                                            if(confi.charAt(val)==';'){
                                                NOMBRE=nombre;
                                            }else{setError("falta ';'", fila, columna);}
                                        }
                                        break;
                                    case "orden":
                                        break;
                                }
                            }else{setError("se esperaba una igualdad ", fila, columna);}
                        }
                    }while(confi.length()>val);
                }else if(!esc.equals("/")){
                    setError("falta '/' ", fila, columna);
                }
            }else{setError("se esperaba escenario", fila, columna);}
                    
        }
    }
    
    private void setError(String error, int fila, int columna){
        columna++;
        String cat=error+"\tfila:"+fila+" columna:"+columna;
        this.error=(this.error.equals(""))?cat:this.error+"\n"+cat;
    }
    private void setToken(String Token, int fila, int columna){
        columna++;
        String cat=Token+"\tfila:"+fila+" columna:"+columna;
        this.Token=(this.Token.equals(""))?cat:this.Token+"\n"+cat;
        
    }
}
