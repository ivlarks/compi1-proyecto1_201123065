
package JuegoConfig;

public class Extras {
    NodoExtras inicio,fin,aux;
    public Extras(){
        inicio=fin=null;
    }
    private Boolean estaVacia(){
        return (inicio==null);
    }
    public void Agregar(String nombre,String tipo, int x,int y){
        if(!estaVacia()){
            aux=fin;
            fin= new NodoExtras(nombre,tipo, x, y, fin, null);
            aux.Siguiente=fin;
            fin.Siguiente=null;
        }else{
            inicio=fin=aux=new NodoExtras(nombre,tipo, x, y);
        }
    }
    
     public String retornaNodos(){
        String cad = "";
        aux=inicio;
        while(aux!=null){
            String Scad="("+aux.nombre+","+aux.tipo+","+aux.x+","+aux.y+")";
            cad=(cad.equals(""))?Scad:cad+"\n"+Scad;
            aux=aux.Siguiente;
        }
        return cad;
    }
    
}
