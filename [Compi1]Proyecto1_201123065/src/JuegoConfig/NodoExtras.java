
package JuegoConfig;

public class NodoExtras {
   
    String nombre;
    int x,y;
    String tipo;
    NodoExtras Siguiente,Anterior;
    
    public NodoExtras(String nombre,String tipo, int x, int y){
        this(nombre,tipo, x, y,null,null);
    }
    
    public NodoExtras(String nombre,String tipo, int x, int y,NodoExtras Siguiente, NodoExtras anterior){
        this.nombre=nombre;
        this.tipo=tipo;
        this.x=x;
        this.y=y;
        this.Siguiente=Siguiente;
        this.Anterior=anterior;
    }
}
