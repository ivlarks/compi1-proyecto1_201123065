package JuegoConfig;

import Nodos.Enemigo;

public class NodoEscenario {
    String nombre,fondo,meta;
    int ancho, alto,mx,my;
    Personaje personaje;
    Pared pared;
    Suelo suelo;
    Extras extra;
    NodoEscenario anterior,siguiente;
    public NodoEscenario(String nombre, String fondo, int ancho, int alto, Personaje personajes,
            Pared paredes,Suelo suelo,Extras extra,String meta, int mx, int my){
        this(nombre, fondo, ancho, alto, personajes, paredes, suelo, extra, meta, mx, my, null,null);
    }
    public NodoEscenario(String nombre, String fondo, int ancho, int alto, Personaje personajes,
            Pared paredes,Suelo suelo,Extras extra,String meta, int mx, int my,NodoEscenario siguiente, NodoEscenario anterior){
        this.nombre=nombre;
        this.fondo=fondo;
        this.extra=extra;
        this.ancho=ancho;
        this.alto=alto;
        this.personaje=personajes;
        this.pared=paredes;
        this.suelo=suelo;
    }
    
    
}
