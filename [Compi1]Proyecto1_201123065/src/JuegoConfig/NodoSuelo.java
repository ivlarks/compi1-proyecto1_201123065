
package JuegoConfig;


public class NodoSuelo {
    
    String nombre;
    int ix,iy,fx,fy;
    NodoSuelo anterior,siguiente;
    public NodoSuelo(String nombre,int ix,int fx,int iy,int fy){
        this(nombre, ix, fx,iy, fy,null,null);
    }
    public NodoSuelo(String nombre,int ix,int fx,int iy,int fy,NodoSuelo anterior,NodoSuelo Siguiente){
        this.nombre=nombre;
        this.ix=ix;
        this.iy=iy;
        this.fx=fx;
        this.fy=fy;
        this.anterior=anterior;
        this.siguiente=Siguiente;
        
    }
}
