
package JuegoConfig;

public class NodoPared {

    String nombre;
    int ix,iy,fx,fy;
    NodoPared anterior,siguiente;
    public NodoPared(String nombre,int ix,int fx,int iy,int fy){
        this(nombre, ix, fx,iy, fy,null,null);
    }
    public NodoPared(String nombre,int ix,int fx,int iy,int fy,NodoPared anterior,NodoPared Siguiente){
        this.nombre=nombre;
        this.ix=ix;
        this.iy=iy;
        this.fx=fx;
        this.fy=fy;
        this.anterior=anterior;
        this.siguiente=Siguiente;
        
    }
    
}
