
package graficos;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;


public class HojaSprite {
    private final int ancho,alto;
    public final int[] pixeles;
    //ruta de la imagen
    public HojaSprite(final String ruta,final int ancho, final int alto,int iniciaX,int iniciaY){
        this.alto=alto;
        this.ancho=ancho;
        
        pixeles= new int[ancho*alto];
        
        try {
            BufferedImage imagen = ImageIO.read(HojaSprite.class.getResource(ruta));
            
            imagen.getRGB(iniciaX, iniciaY, ancho, alto, pixeles, 0, ancho);
        } catch (IOException ex) {
            
        }
    }
    
    public int getAncho(){
        return ancho;
    }
}
